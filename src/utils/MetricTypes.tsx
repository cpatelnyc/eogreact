
export const MetricTypes = Object.freeze({

    OIL_TEMP: { name:'oilTemp', label:'Oil Temp', color: '#57a0ff', onChangeEvent: 'checkedOilTemp'},
    INJ_VALVE_OPEN: { name:'injValveOpen', label:'INJ Valve Open', color: '#1BD82A', onChangeEvent: 'checkedInjValveOpen'},
    CASING_PRESSURE: { name:'casingPressure', label:'Casing Pressure', color: '#ffffa8', onChangeEvent: 'checkedCasingPressure'},
    TUBING_PRESSURE: { name:'tubingPressure', label:'Tubing Pressure', color: '#FFeeee', onChangeEvent: 'checkedTubingPressure'},
    FLARE_TEMP: { name:'flareTemp', label:'Flare Temp', color: '#FFB201', onChangeEvent: 'checkedFlareTemp'},
    WATER_TEMP: { name:'waterTemp', label:'Water Temp', color: '#ee0CFF', onChangeEvent: 'checkedWaterTemp'},
});

export const getMetricTypeByName = (name: string) => {
    switch(name){
        case MetricTypes.OIL_TEMP.name:
            return MetricTypes.OIL_TEMP;

        case MetricTypes.INJ_VALVE_OPEN.name:
            return MetricTypes.INJ_VALVE_OPEN;

        case MetricTypes.CASING_PRESSURE.name:
            return MetricTypes.CASING_PRESSURE;

        case MetricTypes.TUBING_PRESSURE.name:
            return MetricTypes.TUBING_PRESSURE;

        case MetricTypes.FLARE_TEMP.name:
            return MetricTypes.FLARE_TEMP;

        case MetricTypes.WATER_TEMP.name:
            return MetricTypes.WATER_TEMP;
    }

}