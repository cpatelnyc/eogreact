import React, { useEffect, useState } from 'react';
import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend } from 'recharts';
import { useSelector } from 'react-redux';
import Card from './Card';
import { MetricTypes, getMetricTypeByName } from '../utils/MetricTypes';

export default function MyChart() {
  const [dataArr, dataCon] = useState([]);
  const multiData = useSelector(state => state.multipleData.multipleData);
  const injValveData = useSelector(state => state.injValve.injValveData);
  const oilTempData = useSelector(state => state.oilTemp.oilTempData);
  const flareTempData = useSelector(state => state.flareTemp.flareTempData);
  const waterTempData = useSelector(state => state.waterTemp.waterTempData);
  const casingPressureData = useSelector(state => state.casingPressure.casingPressureData);
  const tubingPressureData = useSelector(state => state.tubingPressure.tubingPressureData);
  const activeMetrics = useSelector(state => state.activeMetrics.selectedMetrics);

  const filterByActive = data => {
    for (let i = 0; i < activeMetrics.length; i++) {
      if (data.metric === activeMetrics[i].metricName) {
        return true;
      }
    }
  };

  const dataForChart = dataArr.filter(filterByActive);

  useEffect(() => {
    if (multiData.length > 0) {
      return dataCon([
        {
          metric: MetricTypes.INJ_VALVE_OPEN.name,
          measurements: multiData[0].measurements.concat(injValveData),
        },
        {
          metric: MetricTypes.OIL_TEMP.name,
          measurements: multiData[1].measurements.concat(oilTempData),
        },
        {
          metric: MetricTypes.CASING_PRESSURE.name,
          measurements: multiData[2].measurements.concat(casingPressureData),
        },
        {
          metric: MetricTypes.TUBING_PRESSURE.name,
          measurements: multiData[3].measurements.concat(tubingPressureData),
        },
        {
          metric: MetricTypes.FLARE_TEMP.name,
          measurements: multiData[4].measurements.concat(flareTempData),
        },
        {
          metric: MetricTypes.WATER_TEMP.name,
          measurements: multiData[5].measurements.concat(waterTempData),
        },
      ]);
    }
  }, [injValveData, casingPressureData, flareTempData, multiData, oilTempData, tubingPressureData, waterTempData]);

  return (
    <>
      {activeMetrics.map(i => {
        if (i.metricName === injValveData[0].metric) {
          return (
            <Card
              key={i.metricName}
              color={MetricTypes.INJ_VALVE_OPEN.color}
              metric={MetricTypes.INJ_VALVE_OPEN.label}
              data={`${injValveData[injValveData.length - 1].value}${injValveData[0].unit}`}
            />
          );
        } else if (i.metricName === oilTempData[0].metric) {
          return (
            <Card
              key={i.metricName}
              color={MetricTypes.OIL_TEMP.color}
              metric={MetricTypes.OIL_TEMP.label}
              data={`${oilTempData[oilTempData.length - 1].value} ${oilTempData[0].unit}`}
            />
          );
        } else if (i.metricName === flareTempData[0].metric) {
          return (
            <Card
              key={i.metricName}
              color={MetricTypes.FLARE_TEMP.color}
              metric={MetricTypes.FLARE_TEMP.label}
              data={`${flareTempData[flareTempData.length - 1].value} ${flareTempData[0].unit}`}
            />
          );
        } else if (i.metricName === waterTempData[0].metric) {
          return (
            <Card
              key={i.metricName}
              color={MetricTypes.WATER_TEMP.color}
              metric={MetricTypes.WATER_TEMP.label}
              data={`${waterTempData[waterTempData.length - 1].value} ${waterTempData[0].unit}`}
            />
          );
        } else if (i.metricName === casingPressureData[0].metric) {
          return (
            <Card
              key={i.metricName}
              color={MetricTypes.CASING_PRESSURE.color}
              metric={MetricTypes.CASING_PRESSURE.label}
              data={`${casingPressureData[casingPressureData.length - 1].value} ${casingPressureData[0].unit}`}
            />
          );
        } else if (i.metricName === tubingPressureData[0].metric) {
          return (
            <Card
              key={i.metricName}
              color={MetricTypes.TUBING_PRESSURE.color}
              metric={MetricTypes.TUBING_PRESSURE.label}
              data={`${tubingPressureData[tubingPressureData.length - 1].value} ${tubingPressureData[0].unit}`}
            />
          );
        }
      })}

      <LineChart width={1200} height={400}>
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="datetimeAt" type="category" />
        <YAxis dataKey="value" />
        <Tooltip labelStyle={{ color: "green" }}
            itemStyle={{ color: "rgb(33,33,33)"}}
            formatter={function(value, name) {
              return `${value}`;
            }}
            labelFormatter={function(value) {
              return `label: ${value}`;
            }}/>
        <Legend layout="vertical" verticalAlign="middle" align="right" margin-bottom="10px" />
        {dataForChart.map(i => {
          return (
            <Line
              dataKey="value"
              data={i.measurements}
              name={getMetricTypeByName(i.metric).label}
              key={i.metric}
              stroke={getMetricTypeByName(i.metric).color}
            />
          );
        })}
      </LineChart>
    </>
  );
}
