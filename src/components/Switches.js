import React, { useState } from 'react';
import Switch from '@material-ui/core/Switch';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import { useDispatch, useSelector } from 'react-redux';
import { actions } from '../Features/ActiveMetrics/sliceReducer';

export default function Switches() {
  const [state, setState] = useState({
    checkedInjValveOpen: false,
    checkedOilTemp: false,
    checkedTubingPressure: false,
    checkedFlareTemp: false,
    checkedCasingPressure: false,
    checkedWaterTemp: false,
  });
  const metricTypesData = useSelector(state => state.metricTypes.metricTypesData);
  const timeStamp = useSelector(state => state.heartbeat);
  const dispatch = useDispatch();
  const activeArr = useSelector(state => state.activeMetrics.selectedMetrics);

  const handleChange = name => event => {
    const metric = event.target.value;
    const isChecked = event.target.checked;
    setState({ ...state, [name]: event.target.checked });

    if (isChecked) {
      dispatch(
        actions.active({
          metricName: metric,
          before: timeStamp.current,
          after: timeStamp.past,
        }),
      );
    } else {
      const metricIndex = activeArr.find(element => element.metricName === metric);
      dispatch(actions.remove(metricIndex.metricName));
    }
  };

  return (
    <div>
      <h1>View Data Chart</h1>

      <FormControl component="fieldset">
        <FormGroup aria-label="position" row>
          {metricTypesData[0]?.map(myMetric => (
            <FormControlLabel
              value="top"
              control={
                <Switch
                  checked={!!state[myMetric.onChangeEvent]}
                  onChange={handleChange(myMetric.onChangeEvent)}
                  value={myMetric.name}
                  inputProps={{ 'aria-label': 'primary checkbox' }}
                />
              }
              key={myMetric.name}
              label={myMetric.label}
              labelPlacement="start"
            />
          ))}
        </FormGroup>
      </FormControl>
      <p />
    </div>
  );
}
