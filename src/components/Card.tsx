import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';

export default function SimpleCard(props: { color: any; metric: React.ReactNode; data: React.ReactNode; }) {
  const useStyles = makeStyles({
    card: {
      maxWidth: 150,
      float: 'left',
      marginLeft: 10,
    },
    title: {
      fontSize: 12,
    },
    pos: {
      marginBottom: 10,
    },
    metric: {
      color: props.color,
    },
  });

  const classes = useStyles();

  return (
    <Card className={classes.card}>
      <CardContent>
        <Typography className={classes.title} gutterBottom>
          {props.metric}
        </Typography>
        <Typography className={classes.metric} variant="body2" component="p">
          {props.data}
        </Typography>
      </CardContent>
    </Card>
  );
}
