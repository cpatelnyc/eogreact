import Chip from '@material-ui/core/Chip';
import { withStyles, Theme } from '@material-ui/core/styles';

const cardStyles = (theme: Theme) => ({
  root: {
    background: theme.palette.background.paper,
  },
  label: {
    color: theme.palette.secondary.main,
  },
});
export default withStyles(cardStyles)(Chip);
