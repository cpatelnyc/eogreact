import React from 'react';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';

import Grid from '@material-ui/core/Grid';
import Switches from './Switches';
import MyChart from './MyChart';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
      margin: '12px'
    },
    right: {
      padding: theme.spacing(2),
      textAlign: 'center',
      color: theme.palette.text.secondary,
    },
    left: {
      width: 300,
    },
    img: {
      margin: '12px',
      display: 'block',
      maxWidth: '100%',
      maxHeight: '100%',
    },
  }),
);

export default function CenteredGrid() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Grid container spacing={3}>
        <Grid item xs={2} className={classes.left}>
            <Switches />
        </Grid>
        <Grid item xs={6} className={classes.right}>
            <MyChart />
        </Grid>
      </Grid>
    </div>
  );}