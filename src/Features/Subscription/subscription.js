import React, { useEffect, useCallback } from 'react';
import { useDispatch } from 'react-redux';
import { SubscriptionClient } from 'subscriptions-transport-ws';
import { actions as otActions } from '../OilTemp/reducer';
import { actions as injActions } from '../InjValve/reducer';
import { actions as ftActions } from '../flareTemp/reducer';
import { actions as cpActions } from '../CasingPressure/reducer';
import { actions as tpActions } from '../TubingPressure/reducer';
import { actions as wtActions } from '../WaterTemp/reducer';
import { Provider, useSubscription, createClient, defaultExchanges, subscriptionExchange } from 'urql';
import { MetricTypes } from '../../utils/MetricTypes';
import moment from 'moment';

const subscriptionClient = new SubscriptionClient('wss://react.eogresources.com/graphql', {
  reconnect: true,
  timeout: 20000,
});

const client = createClient({
  url: 'https://react.eogresources.com/graphql',
  exchanges: [
    ...defaultExchanges,
    subscriptionExchange({
      forwardSubscription: operation => subscriptionClient.request(operation),
    }),
  ],
});

const newMessages = `
subscription {
  newMeasurement {metric, at, value, unit}
}
`;

export default () => {
  return (
    <Provider value={client}>
      <Subscriber />
    </Provider>
  );
};

const Subscriber = () => {
  const reducerSwitch = measurement => {
    if (measurement.metric === MetricTypes.OIL_TEMP.name) {
      return dispatch(otActions.oilTempData(measurement));
    } else if (measurement.metric === MetricTypes.INJ_VALVE_OPEN.name) {
      return dispatch(injActions.injValveData(measurement));
    } else if (measurement.metric === MetricTypes.FLARE_TEMP.name) {
      return dispatch(ftActions.flareTempData(measurement));
    } else if (measurement.metric === MetricTypes.WATER_TEMP.name) {
      return dispatch(wtActions.waterTempData(measurement));
    } else if (measurement.metric === MetricTypes.CASING_PRESSURE.name) {
      return dispatch(cpActions.casingPressureData(measurement));
    } else if (measurement.metric === MetricTypes.TUBING_PRESSURE.name) {
      return dispatch(tpActions.tubingPressureData(measurement));
    }
  };

  const dispatch = useDispatch();
  const receiveMeasurement = useCallback(
    measurement => {
      measurement.datetimeAt = moment(measurement.at)
        .format('hh:mm A');
      reducerSwitch(measurement);
    },
    [reducerSwitch],
  );
  const [subscriptionResponse] = useSubscription({ query: newMessages });
  const { data: subscriptionData } = subscriptionResponse;

  useEffect(() => {
    if (!subscriptionData) return;
    receiveMeasurement(subscriptionData.newMeasurement);
  }, [subscriptionData, receiveMeasurement]);

  return null;
};
