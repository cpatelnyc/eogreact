import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Provider, createClient, useQuery } from 'urql';
import LinearProgress from '@material-ui/core/LinearProgress';
import { actions as metricTypesActions } from '../Switches/reducer';
import { getMetricTypeByName } from '../../utils/MetricTypes';

const client = createClient({
  url: 'https://react.eogresources.com/graphql',
});


const metricsQuery = `
  query {
    getMetrics                                                                              
  }
  `;

export default () => {
  return (
    <Provider value={client}>
      <GetMetricTypes />
    </Provider>
  );
};


const GetMetricTypes = () => {
  const dispatch = useDispatch();
  const [metricTypes] = useQuery({
    query: metricsQuery
  });
  const { fetching, data, error } = metricTypes;

  useEffect(() => {
    if (error) {
      return;
    }
    if (!data) {
      return;
    } else {
      let myData = data.getMetrics.map(name => getMetricTypeByName(name));
      dispatch(metricTypesActions.metricTypesData(myData));
    }
  });

  if (fetching) return <LinearProgress />;

  return null;
};
