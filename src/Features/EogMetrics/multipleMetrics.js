import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Provider, createClient, useQuery } from 'urql';
import LinearProgress from '@material-ui/core/LinearProgress';
import { actions } from './sliceReducer';
import { MetricTypes } from '../../utils/MetricTypes';

const client = createClient({
  url: 'https://react.eogresources.com/graphql',
});


const measurementQuery = `
  query($input: [MeasurementQuery]) {
    getMultipleMeasurements(input: $input) {
      metric,
        measurements {
            metric,
            at,
            value,
            unit
        }
    }                                                                                       
  }
  `;

export default () => {
  return (
    <Provider value={client}>
      <MultipleMetrics />
    </Provider>
  );
};


const MultipleMetrics = () => {

  const timeStamp = useSelector(state => state.heartbeat);
  const dispatch = useDispatch();

  const metricSet = [
    {
      metricName: MetricTypes.INJ_VALVE_OPEN.name, 
      before: timeStamp.current,
      after: timeStamp.past,
    },
    {
      metricName: MetricTypes.OIL_TEMP.name,
      before: timeStamp.current,
      after: timeStamp.past,
    },
    {
      metricName: MetricTypes.CASING_PRESSURE.name,
      before: timeStamp.current,
      after: timeStamp.past,
    },
    {
      metricName: MetricTypes.TUBING_PRESSURE.name,
      before: timeStamp.current,
      after: timeStamp.past,
    },
    {
      metricName: MetricTypes.FLARE_TEMP.name,
      before: timeStamp.current,
      after: timeStamp.past,
    },
    {
      metricName: MetricTypes.WATER_TEMP.name,
      before: timeStamp.current,
      after: timeStamp.past,
    },
  ];

  const [measurementRes] = useQuery({
    query: measurementQuery,
    variables: {
      input: metricSet,
    },
  });

  const { fetching, data, error } = measurementRes;

  useEffect(() => {
    if (error) {
      return;
    }
    if (!data) {
      return;
    } else {
      const { getMultipleMeasurements } = data;
      dispatch(actions.multipleData(getMultipleMeasurements));
    }
  });

  if (fetching) return <LinearProgress />;

  return null;
};
