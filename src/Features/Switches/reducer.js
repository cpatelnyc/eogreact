import { createSlice } from 'redux-starter-kit';

const initialState = {
    metricTypesData: [],
};

const slice = createSlice({
  name: 'metricTypesData',
  initialState,
  reducers: {
    metricTypesData: (state, action) => {
      state.metricTypesData = [...state.metricTypesData, action.payload];
    },
  },
});

export const reducer = slice.reducer;
export const actions = slice.actions;
