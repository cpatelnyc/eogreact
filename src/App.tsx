import React from 'react';
import createStore from './store';
import { Provider } from 'react-redux';
import { ToastContainer } from 'react-toastify';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import 'react-toastify/dist/ReactToastify.css';
import Header from './components/Header';
import Wrapper from './components/Wrapper';
import Subscription from './Features/Subscription/subscription';
import MultipleMetrics from './Features/EogMetrics/multipleMetrics';
import MetricTypes from './Features/EogMetricTypes/metricTypes';
import Container from './components/Container';

const store = createStore();
const theme = createMuiTheme({
  palette: {
    type: 'dark',
    primary: {
      main: 'rgb(245, 245, 245)',
    },
    secondary: {
      main: 'rgb(197,208,222)',
    },
    background: {
      default: 'rgb(33,33,33)',
    },
  },
  typography: {
    subtitle1: {
      fontSize: 12,
    },
    body1: {
      fontWeight: 500,
    },
    button: {
      fontStyle: 'italic',
    },
  },
});

const App = () => (
  <MuiThemeProvider theme={theme}>
    <CssBaseline />
    <Provider store={store}>
      <Wrapper>
        <MetricTypes />
        <MultipleMetrics />
        <Subscription />
        <Header />
        <Container />
        <ToastContainer />
      </Wrapper>
    </Provider>
  </MuiThemeProvider>
);

export default App;
